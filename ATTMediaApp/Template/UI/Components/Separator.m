//
//  Seperator.m
//  VIA App
//  Template
//
//  Created by vineeth.thayyil on 09/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "Separator.h"
#import <QuartzCore/QuartzCore.h>

@implementation Separator

+ (UIView *) seperatorWithStyle:(SeperatorStyle) style {
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    view.clipsToBounds = NO;
    view.layer.masksToBounds = NO;
    view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin;

    switch (style) {
        case SeperatorStyleMenuView:
            view.backgroundColor = [ UIColor menuItemSeperatorColor ];
            break;
            
      
        default:
            view.backgroundColor = [ UIColor whiteColor ];
            break;
    }
    
    return view;
}



@end
