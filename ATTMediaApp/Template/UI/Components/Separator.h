//
//  Seperator.h
//  Template
//
//  Created by vineeth.thayyil on 09/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SeperatorStyle) {
    SeperatorStyleMenuView
};

@interface Separator : UIView

+ (Separator *) seperatorWithStyle:(SeperatorStyle) style;

@end
