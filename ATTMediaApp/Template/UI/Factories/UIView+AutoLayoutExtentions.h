//
//  UIView+AutoLayoutExtentions.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 15/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_OPTIONS(unsigned long, ViewEdges){
    /**
     *  Top edge
     */
    ViewTopEdge = 1 << 0,
    /**
     *  Right edge
     */
    ViewRightEdge = 1 << 1,
    /**
     *  Bottom edge
     */
    ViewBottomEdge = 1 << 2,
    /**
     *  Left edge
     */
    ViewLeftEdge = 1 << 3,
    /**
     *  All edges
     */
    ViewAllEdges = ~0UL
};


@interface UIView (AutoLayoutExtentions)



-(void) autoLayoutView;

/**
 *  Center view in a view
 *
 *  @param superview the view we want to center into
 *
 *  @return an array of NSLayoutConstraints
 */
-(NSArray *)centerInView:(UIView*)superview;

/**
 *  Center a view in a given dimension
 *
 *  @param axis the dimension we want the view to be centered in
 *
 *  @return A layout constraint
 */
-(NSLayoutConstraint *)centerInContainerOnAxis:(NSLayoutAttribute)axis;

/**
 *  Pin a view to a combination of edges of its superview, using the layout guides from a UIViewContainer
 *
 *  @param edges          the edges to pin the view to
 *  @param inset          the distance to the superview edge
 *  @param viewController a viewcontroller to get layout guides from
 *
 *  @return an array of NSLayoutContraints
 */
-(NSArray*)pinToSuperviewEdges:(ViewEdges)edges inset:(CGFloat)inset usingLayoutGuidesFrom:(UIViewController*)viewController;

/**
 *  Pin view to the edges of its superview
 *
 *  @param edges the edges to pin the view to
 *  @param inset the distance to the edges
 *
 *  @return an array of NSLayoutConstraints
 */
-(NSArray*)pinToSuperviewEdges:(ViewEdges)edges inset:(CGFloat)inset;



-(NSArray *)centerInXaxisForView:(UIView *)superview xOffset:(CGFloat)xOffset;

-(NSArray *)centerInYaxisForView:(UIView *)superview yOffset:(CGFloat)yOffset;

-(NSArray *)width:(CGFloat)width height:(CGFloat)height;

@end
