//
//  GUIFactoryProvider.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 15/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GUIFactoryProvider : NSObject

+(UIInterfaceOrientationMask)getDeviceSupportedOrientations;

@end
