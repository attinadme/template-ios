//
//  GUIFactoryProvider.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 15/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "GUIFactoryProvider.h"

@implementation GUIFactoryProvider

+(UIInterfaceOrientationMask)getDeviceSupportedOrientations
{
    if([Utils isIpad])
    {
        return UIInterfaceOrientationMaskLandscape;
    }
    else
    {
        return UIInterfaceOrientationMaskPortrait;
    }
}


@end
