//
//  UIView+AutoLayoutExtentions.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 15/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "UIView+AutoLayoutExtentions.h"

@implementation UIView (AutoLayoutExtentions)

-(void) autoLayoutView
{
    self.translatesAutoresizingMaskIntoConstraints = NO;
}

- (NSArray *) centerInView: (UIView*) superview
{
    NSMutableArray *constraints = [ NSMutableArray new ];
    
    [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeCenterX relatedBy: NSLayoutRelationEqual toItem: superview attribute: NSLayoutAttributeCenterX multiplier: 1.0 constant: 0 ] ];
    [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeCenterY relatedBy: NSLayoutRelationEqual toItem: superview attribute: NSLayoutAttributeCenterY multiplier: 1.0 constant: 0 ] ];
    
    [ superview addConstraints: constraints ];
    return [ constraints copy ];
}

- (NSLayoutConstraint *) centerInContainerOnAxis: (NSLayoutAttribute) axis
{
    NSParameterAssert(axis == NSLayoutAttributeCenterX || axis == NSLayoutAttributeCenterY);
    
    UIView *superview = self.superview;
    NSParameterAssert(superview);
    NSLayoutConstraint *constraint = [ NSLayoutConstraint constraintWithItem: self
                                                                   attribute: axis
                                                                   relatedBy: NSLayoutRelationEqual
                                                                      toItem: superview
                                                                   attribute: axis
                                                                  multiplier: 1.0
                                                                    constant: 0.0 ];
    [ superview addConstraint: constraint ];
    return constraint;
}

- (NSArray *) pinToSuperviewEdges: (ViewEdges) edges inset: (CGFloat) inset usingLayoutGuidesFrom: (UIViewController *) viewController
{
    UIView *superview = self.superview;
    NSAssert(superview,@"Can't pin to a superview if no superview exists");
    
    id topItem      = nil;
    id bottomItem   = nil;
    
    if( viewController && [ viewController respondsToSelector: @selector( topLayoutGuide ) ] )
    {
        topItem = viewController.topLayoutGuide;
        bottomItem = viewController.bottomLayoutGuide;
    }
    
    NSMutableArray *constraints = [ NSMutableArray new ];
    
    if( edges & ViewTopEdge )
    {
        id item = topItem ? topItem : superview;
        NSLayoutAttribute attribute = topItem ? NSLayoutAttributeBottom : NSLayoutAttributeTop;
        [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeTop relatedBy: NSLayoutRelationEqual toItem: item attribute: attribute multiplier: 1.0 constant: inset ] ];
    }
    if( edges & ViewLeftEdge )
    {
        [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeLeft relatedBy: NSLayoutRelationEqual toItem: superview attribute: NSLayoutAttributeLeft multiplier: 1.0 constant: inset ] ];
    }
    if( edges & ViewRightEdge )
    {
        [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeRight relatedBy: NSLayoutRelationEqual toItem: superview attribute: NSLayoutAttributeRight multiplier: 1.0 constant: -inset ] ];
    }
    if( edges & ViewBottomEdge )
    {
        id item = bottomItem ? bottomItem : superview;
        NSLayoutAttribute attribute = bottomItem ? NSLayoutAttributeTop : NSLayoutAttributeBottom;
        [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeBottom relatedBy: NSLayoutRelationEqual toItem: item attribute: attribute multiplier: 1.0 constant: -inset ] ];
    }
    
    [ superview addConstraints: constraints ];
    return [ constraints copy ];
}


- (NSArray*) pinToSuperviewEdges: (ViewEdges) edges inset: (CGFloat) inset
{
    return [ self pinToSuperviewEdges: edges inset: inset usingLayoutGuidesFrom: nil ];
}



-(NSArray *)centerInXaxisForView:(UIView *)superview xOffset:(CGFloat)xOffset
{
    NSMutableArray *constraints = [ NSMutableArray new ];
    
    [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeCenterX relatedBy: NSLayoutRelationEqual toItem: superview attribute: NSLayoutAttributeCenterX multiplier: 1.0 constant: xOffset ] ];
    
    
    [ superview addConstraints: constraints ];
    return [ constraints copy ];
}

-(NSArray *)centerInYaxisForView:(UIView *)superview yOffset:(CGFloat)yOffset
{
    NSMutableArray *constraints = [ NSMutableArray new ];
    
    [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeCenterY relatedBy: NSLayoutRelationEqual toItem: superview attribute: NSLayoutAttributeCenterY multiplier: 1.0 constant: yOffset ] ];
    
    [ superview addConstraints: constraints ];
    return [ constraints copy ];
}


-(NSArray *)width:(CGFloat)width height:(CGFloat)height
{
    NSMutableArray *constraints = [ NSMutableArray new ];
    if(width > 0)
        [constraints addObject: [NSLayoutConstraint
                                 constraintWithItem:self
                                 attribute:NSLayoutAttributeWidth
                                 relatedBy:NSLayoutRelationEqual
                                 toItem: nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                 multiplier:1.0f
                                 constant:width]];
    if(height > 0)
        [constraints addObject: [NSLayoutConstraint
                                 constraintWithItem:self
                                 attribute:NSLayoutAttributeHeight
                                 relatedBy:NSLayoutRelationEqual
                                 toItem: nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                 multiplier:1.0f
                                 constant:height]];
    [self addConstraints: constraints ];
    return [ constraints copy ];
    
}

@end
