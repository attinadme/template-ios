//
//  DetailsTableDelegate.h
//  Template
//
//  Created by vineeth.thayyil on 08/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DetailsTableDelegate : NSObject <UITableViewDelegate>

@end
