//
//  DetailsTableDataSource.m
//  Template
//
//  Created by vineeth.thayyil on 08/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "DetailsTableDataSource.h"
#import "DetailsTableCell.h"

static NSString *kReuseId = @"DetailsCell";

@implementation DetailsTableDataSource



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    DetailsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:kReuseId];
    
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([DetailsTableCell class]) owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    
    cell.textLabel.text = @"Sample text";
    
    return cell;
}

@end
