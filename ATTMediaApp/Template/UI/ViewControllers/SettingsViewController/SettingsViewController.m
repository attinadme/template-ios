//
//  SettingsViewController.m
//  ATTMediaApp
//
//  Created by emel.elias on 16/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "SettingsViewController.h"
#import "NavigationManager.h"
#import "ConfigurationFactory.h"
#import "Configuration.h"
#import "SettingsHeaderItem.h"
#import "SettingsRowItem.h"
#import "UIView+AutoLayoutExtentions.h"

typedef NS_ENUM(NSInteger, SettingsCellType) {
    SettingsCellTypeInfo,
    SettingsCellTypeInfoNavigate,
};

@interface SettingsViewController () <UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *settingsContainerTableView;
@property (nonatomic,strong) NSArray *sectionContent;
@property (nonatomic,strong) UIButton *signOutButton;

@end

@implementation SettingsViewController


-(NSMutableArray*) createDisplayContent {
    
    NSMutableArray *displayContent = [[NSMutableArray alloc] init];
    
    
    if (1) {
        
        SettingsHeaderItem *userAccountDetailsitem = [[SettingsHeaderItem alloc] init];
        userAccountDetailsitem.title               = [[ConfigurationFactory configuration] fetchMessageForKey:@"settings_title_account_info"];
        
        userAccountDetailsitem.subItems = [[NSMutableArray alloc] init];
        SettingsRowItem *emailItem = [[SettingsRowItem alloc] initWithTitle:[[ConfigurationFactory configuration] fetchMessageForKey:@"settings_item_title_email"]
                                                                   subTitle:@"Email"
                                                                  urlString:@""
                                                                   itemtype:SettingsItemtypeDefault];
        
        
        SettingsRowItem *nameItem = [[SettingsRowItem alloc] initWithTitle:[[ConfigurationFactory configuration] fetchMessageForKey:@"settings_item_title_name"]
                                                                  subTitle:@"Name"
                                                                 urlString:@""

                                                                   itemtype:SettingsItemtypeDefault];
        
        [userAccountDetailsitem.subItems addObject:emailItem];
        [userAccountDetailsitem.subItems addObject:nameItem];

        [displayContent addObject:userAccountDetailsitem];
        
        
        
    }
    
    /**Application details*/
    SettingsHeaderItem *applicationdetailsItem = [[SettingsHeaderItem alloc] init];
    applicationdetailsItem.title               = [[ConfigurationFactory configuration] fetchMessageForKey:@"settings_title_application_info"];
    applicationdetailsItem.subItems = [[NSMutableArray alloc] init];
    
    SettingsRowItem *aboutUsItem = [[SettingsRowItem alloc] initWithTitle:[[ConfigurationFactory configuration] fetchMessageForKey:@"settings_item_title_about_us"]
                                                                 subTitle:@""
                                                                urlString:@""
                                                                 itemtype:SettingsItemtypeWebview];
    
    [applicationdetailsItem.subItems addObject:aboutUsItem];
    
    
    SettingsRowItem *privacyPolicyItem = [[SettingsRowItem alloc] initWithTitle:[[ConfigurationFactory configuration] fetchMessageForKey:@"settings_item_title_privacy_policy"]
                                                                       subTitle:@""
                                                                      urlString:@""
                                                                 itemtype:SettingsItemtypeWebview];
    
    [applicationdetailsItem.subItems addObject:privacyPolicyItem];
    
    
    [displayContent addObject:applicationdetailsItem];
    
    
    
    return displayContent;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateUI];
    // Do any additional setup after loading the view
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Update UI -

-(void) updateUI {
    
    static NSString *kSettingsTitleKey  = @"settings_page_title";
    self.title                          = [[ConfigurationFactory configuration] fetchMessageForKey:kSettingsTitleKey];
    self.sectionContent                 = [self createDisplayContent];
    [self.settingsContainerTableView setTableFooterView:[self tableFooterView]];

    [self.settingsContainerTableView reloadData];
}



-(UIView*)tableFooterView {
    UIView *tableFooterView = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, self.settingsContainerTableView.frame.size.width, 100)];
    
    if (1) {
        [tableFooterView addSubview:self.signOutButton];
        [self.signOutButton centerInXaxisForView:tableFooterView xOffset:0.0f];
        [self.signOutButton pinToSuperviewEdges:ViewBottomEdge inset:22];
    }
    
    
    return tableFooterView;
}

-(UIButton*)signOutButton {
    
    if (!_signOutButton) {
        _signOutButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_signOutButton autoLayoutView];
        _signOutButton.backgroundColor = [UIColor signOutButtonBackgroundColour];
        [_signOutButton width:197 height:35];
        [_signOutButton setTitle:[[ConfigurationFactory configuration] fetchMessageForKey:@"settings_button_text_sign_out"] forState:UIControlStateNormal];
      
        [_signOutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_signOutButton addTarget:self action:@selector(logOut) forControlEvents:UIControlEventTouchUpInside];
        [_signOutButton setCornerRadiusWithWidth:3.0f];
    }
    
    return _signOutButton;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return self.sectionContent.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 55.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    SettingsHeaderItem *headerItem = self.sectionContent[ section ];
    return [ headerItem.subItems count ];
}


- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    SettingsHeaderItem *headerItem = self.sectionContent[ section ];
    return headerItem.title;
}

static NSString *kCellIdentifier = @"cellIdentifier";

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = (UITableViewCell *) [ tableView dequeueReusableCellWithIdentifier: kCellIdentifier ];
    
    if( !cell )
    {
        cell = [ [ UITableViewCell alloc ] initWithStyle: UITableViewCellStyleValue1 reuseIdentifier: kCellIdentifier ];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.detailTextLabel.textAlignment = NSTextAlignmentRight;
        [cell.textLabel setTextColor:[UIColor blackColor]];
        [cell.detailTextLabel setTextColor:[UIColor blackColor]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone ;
        
    }
    SettingsHeaderItem *headerItem = self.sectionContent[ indexPath.section ];
    SettingsRowItem *item= headerItem.subItems[indexPath.row];
    cell.textLabel.text = item.mainTitle;
    cell.detailTextLabel.text = item.subTitle;
    
    [cell.detailTextLabel setTextColor:[UIColor blackColor]];
    
    if (item.itemType == SettingsItemtypeDefault) {
        
    } else if (item.itemType == SettingsItemtypeWebview){
//        cell.detailTextLabel.textColor = [UIColor colorWithRed:6.0f/255.0f green:83.0f/255.0f blue:185.0f/255.0f alpha:1.0f];
    }
    
    
    return cell;
}



- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsHeaderItem *headerItem = self.sectionContent[ indexPath.section ];
    
    SettingsRowItem *item          = headerItem.subItems[indexPath.row];
    
    if (item.itemType == SettingsItemtypeWebview) {
        
        NSMutableDictionary *metaData = [[NSMutableDictionary alloc] init];
        [metaData setObject:item.urlString safetlyForKey:kUrlToLoadInWebView];
      
        
        NavigationDestination *destination = [[NavigationDestination alloc] initWithType:NavigationDestinationTypeWebView
                                                                                metadata:metaData];
        
        [[NavigationManager sharedInstance] navigateToDestination:destination];
        
        
    }
    
    
   

    
}


#pragma mark - LogOut -

-(void)logOut {
    
}




@end
