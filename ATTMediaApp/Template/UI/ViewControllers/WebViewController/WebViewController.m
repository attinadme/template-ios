//
//  WebViewController.m
//  ATTMediaApp
//
//  Created by emel.elias on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "WebViewController.h"
#import "UIView+VFLExtensions.h"

@interface WebViewController ()
@property(nonatomic,strong) UIWebView *containerWebView;
@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.containerWebView = [[UIWebView alloc] init];
    [self.view autoLayoutAddSubview:self.containerWebView];
    [self.view addConstraintsForFillView:self.containerWebView];
    if(self.data[@"url"])
    {
    [self.containerWebView loadRequest:[NSURLRequest requestWithURL:self.data[@"url"]]];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
