//
//  ViewController.m
//  Template
//
//  Created by vineeth.thayyil on 06/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//
#import "HomeViewController.h"
#import "HomeDataService.h"
#import "DetailsTableDelegate.h"
#import "DetailsTableDataSource.h"
#import "Configuration.h"
#import "ConfigurationFactory.h"

@interface HomeViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *detailsWebView;
@property (weak, nonatomic) IBOutlet UITableView *detailsTableView;

@property (nonatomic,strong) HomeDataService        *dataService;
@property (nonatomic,strong) DetailsTableDataSource *detailsTableDataSource;
@property (nonatomic,strong) DetailsTableDelegate   *detailsTableDelegate;

@end

@implementation HomeViewController

- (HomeDataService*)dataService {
    
    if (!_dataService) {
        _dataService = [[HomeDataService alloc] init];
    }
    
    return _dataService;
    
}

- (DetailsTableDelegate*) detailsTableDelegate {
    
    if (!_detailsTableDelegate) {
        _detailsTableDelegate = [[DetailsTableDelegate alloc] init];
    }
    return _detailsTableDelegate;
    
}

- (DetailsTableDataSource*) detailsTableDataSource {
    
    if (!_detailsTableDataSource) {
        _detailsTableDataSource = [[DetailsTableDataSource alloc] init];
    }
    
    return _detailsTableDataSource;
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUpUi];
    [self fetchDetails];
    [self displayDetails];
    
    [[ConfigurationFactory configuration] fetchRailListForPageID:@"home"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)dealloc {
    
}

#pragma mark - SetUp UI Elements -

-(void) setUpUi {
    self.detailsTableView.delegate   = self.detailsTableDelegate;
    self.detailsTableView.dataSource = self.detailsTableDataSource;
    [self.detailsTableView reloadData];
}

- (HeaderStyle) headerStyle
{
    return HeaderStyleRevealButton;
}
#pragma mark - API Calls -

-(void) fetchDetails {
    
    [self.dataService fetchDetailsOnCompletion:^(id response, BOOL status, NSError *error) {
        
    }];
    
}

#pragma mark - Display detils -

-(void) displayDetails {
    
    NSString *urlAddress = @"https://www.google.com";
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.detailsWebView loadRequest:requestObj];
}


@end
