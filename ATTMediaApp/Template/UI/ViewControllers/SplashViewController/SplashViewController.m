//
//  SplashViewController.m
//  Template
//
//  Created by vineeth.thayyil on 09/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "SplashViewController.h"

@interface SplashViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *splashLogo;

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUi];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setUpUi {
    [self.splashLogo setImage:[UIImage imageNamed:@"Logo"]];
}


@end
