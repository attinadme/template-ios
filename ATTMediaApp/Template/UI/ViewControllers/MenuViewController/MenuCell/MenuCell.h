//
//  MenuCell.h
//  Template
//
//  Created by vineeth.thayyil on 14/09/16.
//  Copyright © 2016 vineeth.thayyil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *menuImage;
@property (nonatomic,weak) IBOutlet UILabel *menuText;

@end
