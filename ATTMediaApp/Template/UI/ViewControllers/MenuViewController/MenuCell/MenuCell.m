//
//  MenuCell.m
//  Template
//
//  Created by vineeth.thayyil on 14/09/16.
//  Copyright © 2016 vineeth.thayyil. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
