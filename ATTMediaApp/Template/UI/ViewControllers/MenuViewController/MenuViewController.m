//
//  MenuViewController.m
//  Template
//
//  Created by vineeth.thayyil on 10/09/16.
//  Copyright © 2016 vineeth.thayyil. All rights reserved.
//

#import "MenuViewController.h"
#import "NavigatinMenuViewController.h"
#import "UIView+VFLExtensions.h"
#import "NavigationManager.h"

@interface MenuViewController ()

@property (nonatomic,strong) NavigatinMenuViewController *menuTableViewController;
@property (nonatomic,strong) UIButton *headerButton;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor blackColor];// [UIColor colorWithWhite:0.114 alpha:1.000];
    [self initViews];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initViews
{
    UIView *headerView = [self headerView];
    UIView *footerView = [self footerView];
    UIView *menutable = [self menuTableView];
    
    float width = [Utils isIpad] ? 300 : [UIScreen mainScreen].bounds.size.width - 70;
    
    [self.view autoLayoutAddSubview:headerView];
    [self.view autoLayoutAddSubview:menutable];
    [self.view autoLayoutAddSubview:footerView];
    
    [self.view addConstraintsOnAxis1:nil axis2:@"H:|-(0)-[V1(M1)]" metrics:@[@(width)] views:@[headerView]];
    [self.view addConstraintsOnAxis1:nil axis2:@"H:|-(0)-[V1(M1)]" metrics:@[@(width)] views:@[menutable]];
    [self.view addConstraintsOnAxis1:nil axis2:@"H:|-(0)-[V1(M1)]" metrics:@[@(width)] views:@[footerView]];
    //20 to accomadate status bar
    [self.view addConstraintsOnAxis1:nil axis2:@"V:|-(0)-[V1]-(0)-[V2]-(0)-[V3(40)]-(0)-|" metrics:nil views:@[headerView,menutable,footerView]];
}

-(UIView*)menuTableView
{
    self.menuTableViewController = [ [ NavigatinMenuViewController alloc ] init ];
    float width = [Utils isIpad] ? 300 : [UIScreen mainScreen].bounds.size.width - 70;
    
    self.menuTableViewController.view.frame = CGRectMake(0, 0, width, CGRectGetHeight(self.view.frame) );
    [self addChildViewController:self.menuTableViewController];
    [self.menuTableViewController didMoveToParentViewController:self];
    
    return self.menuTableViewController.view;
}

-(UIView*)headerView
{
    UIView *headerContainerView = [[UIView alloc] init];
    headerContainerView.backgroundColor =  [UIColor menuBackgroundColor];
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView setImage:[UIImage imageNamed:@"userIcon"]];
    [headerContainerView autoLayoutAddSubview:imageView];
    self.headerButton = [[UIButton alloc] init];
    [self.headerButton addTarget:self action:@selector(headerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.headerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self setHeaderButtonTitle];
    [self.headerButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.headerButton.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];

    [headerContainerView autoLayoutAddSubview:self.headerButton];
    UIView *separator = [[UIView alloc] init];
    separator.backgroundColor = [UIColor lightGrayColor];
    [headerContainerView autoLayoutAddSubview:separator];
    [headerContainerView addConstraintsOnAxis1:nil axis2:@"V:|-(30)-[V1(20)]-(10)-|" metrics:nil views:@[imageView]];
    [headerContainerView addConstraintsOnAxis1:nil axis2:@"V:|-(20)-[V1]-(0)-|" metrics:nil views:@[self.headerButton]];
    [headerContainerView addConstraintsOnAxis1:nil axis2:@"V:|-[V1(1)]-0-|" metrics:nil views:@[separator]];
    [headerContainerView addConstraintsOnAxis1:nil axis2:@"H:|-(0)-[V1]-(0)-|" metrics:nil views:@[separator]];
    [headerContainerView addConstraintsOnAxis1:nil axis2:@"H:|-(15)-[V1(20)]-(10)-[V2]-(0)-|" metrics:nil views:@[imageView, self.headerButton]];
    return headerContainerView;
}

-(UIView*)footerView
{
    UIView *footerContainerView = [[UIView alloc] init];
  footerContainerView.backgroundColor = [UIColor menuBackgroundColor];
    UIButton *footerButton = [[UIButton alloc] init];
    [footerButton addTarget:self action:@selector(footerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [footerButton setImage:[UIImage imageNamed:@"ic_settings"] forState:UIControlStateNormal];
    [footerContainerView autoLayoutAddSubview:footerButton];
  footerButton.highlighted = YES;
    [footerContainerView addConstraintsOnAxis1:nil axis2:@"H:[V1(30)]" metrics:nil views:@[footerButton]];
    [footerContainerView positionCenter:footerButton];
    return footerContainerView;
}


-(void)footerButtonClicked:(id)sender
{
    [[NavigationManager sharedInstance] navigateToView:NavigationDestinationTypeSettings withData:nil isforcePush:NO];
}

-(void)headerButtonClicked:(id)sender
{

}

-(void) selectHomeForce {
    [self.menuTableViewController selectHomeForce];
}

-(void)setHeaderButtonTitle {
        [self.headerButton setTitle:@"" forState:UIControlStateNormal];
}

- (void)userStatusDidChanged {
    [self setHeaderButtonTitle];
}

@end
