//
//  NavigatinMenuViewController.m
//  Template
//
//  Created by vineeth.thayyil on 10/09/16.
//  Copyright © 2016 vineeth.thayyil. All rights reserved.
//

#import "NavigatinMenuViewController.h"
#import "MenuCell.h"
#import "MenuItem.h"
#import "AppDelegate.h"
#import "NavigationManager.h"
#import "MBProgressHUD.h"
#import "MenuHeaderCell.h"
#import "Separator.h"
#import "UIView+AutoLayoutExtentions.h"

static NSString *reuseId = @"reuseIdentifier";
static CGFloat const kRegularCellHeight    = 44.0f;

@interface NavigatinMenuViewController ()

@property (nonatomic,strong) NSMutableArray *displayArray ;
@property (nonatomic,strong) NSIndexPath *selectedIndexpath;

@end

@implementation NavigatinMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createDisplyArray];
    
    self.tableView.backgroundView.backgroundColor = [UIColor menuTableBackgroundColor];
    self.tableView.backgroundColor = [UIColor menuTableBackgroundColor];
    self.tableView.separatorColor = [ UIColor clearColor ];
    [ self setClearsSelectionOnViewWillAppear: NO ];
    [self.tableView reloadData];
    [self selectHomeForce];

}



-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self createDisplyArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) selectHomeForce {
    
//    
//    self.selectedIndexpath = [NSIndexPath indexPathForRow:0 inSection:1];
//    [ self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]
//                                 animated: NO scrollPosition: UITableViewScrollPositionNone ];
}


-(void) createDisplyArray {
  
    _displayArray       = [[NSMutableArray alloc] init];
  
    MenuItem *menuItem  = [[MenuItem alloc] init];
    menuItem.image      = [UIImage imageNamed:@"image"];
    menuItem.title      = @"Home";
    [_displayArray addObject:menuItem];
    menuItem.hasChildren = NO;

    MenuItem *mostWatched  = [[MenuItem alloc] init];
    mostWatched.title      = @"Most Watched";
    MenuItem *mostPopular  = [[MenuItem alloc] init];
    mostPopular.title      = @"Most popular";
    
    MenuItem *login  = [[MenuItem alloc] init];
    login.title      = @"Login/Register";
  
  
  MenuItem *menuItem2  = [[MenuItem alloc] init];
  menuItem2.image      = [UIImage imageNamed:@"image"];
  menuItem2.title      = @"Movie";
  menuItem2.hasChildren = NO;

  [_displayArray addObject:menuItem2];

  
  MenuItem *menuItem3  = [[MenuItem alloc] init];
  menuItem3.image      = [UIImage imageNamed:@"image"];
  menuItem3.title      = @"TV Show";
  menuItem3.hasChildren = NO;

  [_displayArray addObject:menuItem3];

  

  
//    menuItem.subMenuItems = [NSArray arrayWithObjects:login, mostPopular,mostWatched, nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_displayArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    MenuItem *menuItem  = [_displayArray objectAtIndex:section];
    if (menuItem.isOpened) {
        return menuItem.subMenuItems.count;
    }
    return 0;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return kRegularCellHeight;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kRegularCellHeight)];
    
    MenuHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier: kMenuHeaderCell];
    
    if (!cell)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MenuHeaderCell class]) owner:self options:nil];
        cell                = [nib objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        [cell.cellSelectionButton addTarget:self
                                     action:@selector(headerClicked:)
                           forControlEvents:UIControlEventTouchUpInside];
    }

  [headerView addSubview:cell.contentView];
    [cell.contentView autoLayoutView];
    [cell.contentView pinToSuperviewEdges:ViewAllEdges inset:0.0f];

    cell.cellSelectionButton.tag    = section;
        MenuItem *item              = self.displayArray[ section ];
    cell.menuTitle.text             = [item title];
   
    
    if (item.hasChildren) {
        cell.expandIcon.hidden = NO;
      
    }else{
        cell.expandIcon.hidden = YES;
    }
  if (item.isOpened) {
    [cell setExpansionStyle:TableExpansionStyleExpanded animated:YES];
  }else{
    [cell setExpansionStyle:TableExpansionStyleCollapsed animated:YES];
    
  }
  
    

    return headerView;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {


    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId ];
    
    if (cell == nil) {

        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MenuCell class]) owner:self options:nil];
        cell = (MenuCell *)[nib objectAtIndex:0];
        UIView *view = [[UIView alloc] initWithFrame:cell.bounds];
        view.backgroundColor        = [UIColor whiteColor];
        cell.selectedBackgroundView = view;
        cell.backgroundColor        = [UIColor clearColor];
        
    }
    
    MenuItem *mainItem       = [_displayArray objectAtIndex:indexPath.section];
    MenuItem *subItem        = [mainItem.subMenuItems objectAtIndex:indexPath.row];

    cell.menuText.text   = subItem.title;
    cell.menuImage.image = subItem.image;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kRegularCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - Home Page -

-(void) moveToCustompageWithTitle:(NSString*)title {
    
  NavigationDestination *destination = [[NavigationDestination alloc] initWithType:NavigationDestinationTypeHome metadata:[NSDictionary dictionaryWithObject:title forKey:kNavigationTitle]];
  [[NavigationManager sharedInstance] navigateToDestination:destination];
}

#pragma mark - Helper methords


//methods for expanding and collapsing sections

-(void)headerClicked:(UIButton*)sender{
    UIButton *button = (UIButton*)sender;
    MenuItem *item          = [_displayArray objectAtIndex:button.tag];
   item.isOpened = !item.isOpened;
    if (item.hasChildren) {
    }else{
        [[NavigationManager sharedInstance] hideMenu];
        [self moveToCustompageWithTitle:item.title];
    }
  [self.tableView reloadData];

}

@end
