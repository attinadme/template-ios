//
//  MenuHeaderCell.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 15/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "MenuHeaderCell.h"
#import "Separator.h"


static NSString *const kExpandSectionImage = @"arrow_expand";
static NSString *const kCloseSectionImage  = @"arrow_collapse";

@interface MenuHeaderCell ()


@property (nonatomic, readonly) TableExpansionStyle expansionStyle;

@end

@implementation MenuHeaderCell


- (void) setExpansionStyle: (TableExpansionStyle) expansionStyle animated: (BOOL) animated
{
    if( _expansionStyle != expansionStyle )
    {
        _expansionStyle = expansionStyle;
        [ self updateExpandButton ];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.menuTitle.textColor = [UIColor grayColor];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) updateExpandButton
{
    if( self.expansionStyle == TableExpansionStyleExpanded )
    {
        self.expandIcon.image = [ UIImage imageNamed: kCloseSectionImage ];
        self.contentView.backgroundColor  = [ UIColor expandedMenuItemBackgroundColor ];
    }
    else
    {
        self.expandIcon.image = [ UIImage imageNamed: kExpandSectionImage ] ;
        self.contentView.backgroundColor = [ UIColor menuTableBackgroundColor ];
    }
}

- (UIView *) selectedBackground
{
    static UIView *returnValue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        returnValue = [ [ UIView alloc ] init ];
        returnValue.backgroundColor = [ UIColor selectedColor ];
    });
    return returnValue;
}


@end
