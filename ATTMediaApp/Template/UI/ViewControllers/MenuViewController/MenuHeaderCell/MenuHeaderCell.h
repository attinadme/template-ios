//
//  MenuHeaderCell.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 15/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM( NSUInteger, TableExpansionStyle )
{
    TableExpansionStyleCollapsed = 0,
    TableExpansionStyleExpanded
};

static NSString *const kMenuHeaderCell = @"MenuHedaerCell";


@interface MenuHeaderCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *menuTitle;
@property (nonatomic, strong) IBOutlet UIImageView *menuImage;
@property (nonatomic, strong) IBOutlet UIButton *cellSelectionButton;
@property (nonatomic, strong) IBOutlet UIImageView *expandIcon;


- (void) setExpansionStyle: (TableExpansionStyle) expansionStyle animated: (BOOL) animated;

@end
