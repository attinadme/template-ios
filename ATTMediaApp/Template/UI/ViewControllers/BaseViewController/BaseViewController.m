//
//  BaseViewController.m
//  Template
//
//  Created by vineeth.thayyil on 09/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "BaseViewController.h"
#import "GUIFactoryProvider.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (HeaderStyle) headerStyle {
    return HeaderStyleNone;
}


-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return [GUIFactoryProvider getDeviceSupportedOrientations];
}


@end
