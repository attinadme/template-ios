//
//  MaintenanceViewController.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 21/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "MaintenanceViewController.h"
#import "AppDelegate.h"
#import "Configuration.h"
#import "ConfigurationFactory.h"

@interface MaintenanceViewController ()


@property (nonatomic, weak) IBOutlet UILabel *maintenanceMsgLabel;

@end

@implementation MaintenanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showMaintenanceMessage];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) showMaintenanceMessage {
    
    static NSString *kMaintenanceMessage    = @"maintenaceMsg";
    static NSString *kMaintenanceMessageKey = @"app_maintenance_msg";
    
    NSString *message = self.data [kMaintenanceMessage];
    
    if (!message) {
        message = [[ConfigurationFactory configuration] fetchMessageForKey:kMaintenanceMessageKey];
    }
    
    [self.maintenanceMsgLabel setText:message];
}



#pragma mark - IBActions -

-(IBAction)onRetryButtonPress:(id)sender {
    
    AppDelegate *delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [delegate initializeAppLauncher];
    
    
}



@end
