//
//  RailModel.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 16/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>



//{
//    "count": 1,
//    "query": "asset://euro_2016",
//    "rail_id": "home_rail_3",
//    "rail_type": "banner",
//    "rail_title": "Rail 3"
//}


typedef NS_ENUM(NSUInteger, HomeRailTemplate) {
    HomeRailTemplatePortrait,
    HomeRailTemplateLandScape,
    HomeRailTemplateBanner
};




@interface RailModel : NSObject


@property (nonatomic,strong) NSString *railId;
@property (nonatomic,strong) NSString *railType;
@property (nonatomic,strong) NSString *railTitle;
@property (nonatomic,strong) NSString *railQuery;
@property (nonatomic,strong) NSString *count;

-(HomeRailTemplate) homeRailTemplate;

@end
