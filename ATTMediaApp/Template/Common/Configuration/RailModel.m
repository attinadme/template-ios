//
//  RailModel.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 16/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "RailModel.h"

@implementation RailModel

-(HomeRailTemplate) homeRailTemplate {

    HomeRailTemplate railTemplateType = HomeRailTemplatePortrait;
    
    static NSString *kBanner    = @"banner";
    static NSString *kLandScape = @"landscape";
    static NSString *kPortrait  = @"portrait";

    if ([[_railType lowercaseString] isEqualToString:[kBanner lowercaseString]]) {
        return HomeRailTemplateBanner;
    }else if ([[_railType lowercaseString] isEqualToString:[kLandScape lowercaseString]]) {
        return HomeRailTemplateLandScape;
    }else if ([[_railType lowercaseString] isEqualToString:[kPortrait lowercaseString]]) {
        return HomeRailTemplatePortrait;
    }

    return railTemplateType;
}



@end
