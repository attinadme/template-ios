//
//  Configuration.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 15/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Configuration : NSObject


@property (nonatomic,strong) NSMutableDictionary *configValues;



/**
 *  For fetching list of rails for a particular pageId
 *
 *  @param pageID Unique PageId
 *
 *  @return List of rails mapped into model class 'RailModel'
 */
-(NSMutableArray*) fetchRailListForPageID:(NSString*)pageID;


/**
 *  For fetching Language file url
 *
 *  @param key Unique language key as string
 *
 *  @return Language file URL as string
 */

-(NSString*) fetchLanguageUrlForKey:(NSString*) key;


/**
 * For fetching messages configured in server. If message is not configured will display the details from local file
 */
-(NSString*) fetchMessageForKey:(NSString*)key;

@end
