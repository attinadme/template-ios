//
//  ConfigurationMapper.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 16/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "ConfigurationMapper.h"

@implementation ConfigurationMapper


+(NSDictionary*) mappingInfoForRailModel  {

    NSMutableDictionary *mappingObjectDetail = [[NSMutableDictionary alloc] init];
    
    [mappingObjectDetail setObject:@"rail_id" forKey:@"railId"];
    [mappingObjectDetail setObject:@"rail_title" forKey:@"railTitle"];
    [mappingObjectDetail setObject:@"query" forKey:@"railQuery"];
    [mappingObjectDetail setObject:@"rail_type" forKey:@"railType"];

    return mappingObjectDetail;
    
}


@end
