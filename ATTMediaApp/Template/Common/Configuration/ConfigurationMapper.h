//
//  ConfigurationMapper.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 16/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigurationMapper : NSObject


+(NSDictionary*) mappingInfoForRailModel ;

@end
