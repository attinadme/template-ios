//
//  ConfigurationFactory.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 15/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Configuration;

@interface ConfigurationFactory : NSObject

+ (Configuration *) configuration;

@end
