//
//  ConfigurationFactory.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 15/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "ConfigurationFactory.h"
#import "Configuration.h"

@implementation ConfigurationFactory

+ (Configuration *) configuration
{
    static Configuration *cachedConfiguration = nil;
    static dispatch_once_t linearOncePredicate;
    dispatch_once(&linearOncePredicate, ^{
        cachedConfiguration = [ [ Configuration alloc ] init];
    });
    
    return cachedConfiguration;
}



@end
