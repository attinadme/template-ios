//
//  Configuration.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 15/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "Configuration.h"
#import "NSArray+ListArray.h"
#import "NSDictionary+ObjectRepresentation.h"
#import "ConfigurationMapper.h"
#import "RailModel.h"


@interface Configuration ()

@property (nonatomic ,strong) NSMutableDictionary *appMessages;

@end

static NSString *kConfigPages       = @"config_pages";
static NSString *kConfigBands       = @"config_bands";
static NSString *kIdKey             = @"id";
static NSString *kBands             = @"bands";
static NSString *kRailIdKey         = @"rail_id";

@implementation Configuration


-(instancetype) init {
    
    
    if (self = [super init]) {
    
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"configuration" ofType:@"json"];
        NSData *fileData = [NSData dataWithContentsOfFile:filePath];
        NSMutableDictionary *configData = [NSJSONSerialization JSONObjectWithData:fileData
                                                                          options:0
                                                                            error:nil];;
        
        _configValues  = configData;
    }
    
    return self;
}


#pragma mark - Fetch  Messages -

-(NSString*) fetchMessageForKey:(NSString*)key {
    
    
    if (_appMessages[key]) {
        return _appMessages[key];
    }
    
    return [self fetchMessageFromLocalDictForKey:key];
}


-(NSString *)fetchMessageFromLocalDictForKey:(NSString *)key{
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"en_US" ofType:@"json"];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *app_messages = [NSJSONSerialization JSONObjectWithData:fileData
                                                                 options:0
                                                                   error:nil];
    
    return app_messages[key]?:key;
}



#pragma mark - Page -

-(NSArray*) fetchRailDetailsForPageId:(NSString*)pageId {
    
    NSMutableArray *pagesArray = _configValues[kConfigPages];
    
    NSArray *pageElements = [Utils performCaseInSensitiveComparisonOn:pagesArray forKey:kIdKey withValue:pageId];
    
    if (pageElements.count) {
        
        NSDictionary *pageDetails           = pageElements.firstObject;
        
        NSMutableArray *bandList            = pageDetails[kBands];
        
        NSArray *bandIds                    = [bandList valueForKey:kIdKey];
        
        NSMutableArray *bandDetailsArray = [[NSMutableArray alloc] init];
        
        for (NSString *bandId in bandIds) {
            
            NSArray *filtered = [Utils performCaseInSensitiveComparisonOn:_configValues[kConfigBands] forKey:kRailIdKey withValue:bandId];
            
            if (filtered.count) {
                [bandDetailsArray addObject:filtered.firstObject];
            }
        }
        
        return bandDetailsArray;
    }
    
    return nil;
}


-(NSMutableArray*) fetchRailListForPageID:(NSString*)pageID{
    
    
    
    NSArray *railDetails            = [self fetchRailDetailsForPageId:pageID];
    NSMutableArray *railModelList   = [railDetails objectRepresentationForClass:NSStringFromClass([RailModel class]) customKeys:[ConfigurationMapper mappingInfoForRailModel]];
    
    
    return railModelList;
}

#pragma mark - Language -

-(NSString*) fetchLanguageUrlForKey:(NSString*) key {
    return @"";
}



@end
