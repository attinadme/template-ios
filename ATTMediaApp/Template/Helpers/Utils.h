//
//  Utils.h
//  Template
//
//  Created by vineeth.thayyil on 06/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject


+ (BOOL)isIpad;


+(NSArray*) performCaseInSensitiveComparisonOn:(NSArray*)array
                                        forKey:(NSString*)key
                                     withValue:(NSString*)value;

@end
