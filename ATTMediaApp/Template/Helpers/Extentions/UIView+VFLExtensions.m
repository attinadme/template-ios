//
//  UIView+VFLExtensions.m


#import "UIView+VFLExtensions.h"

@implementation UIView (VFLExtensions)

- (void)autoLayoutAddSubview:(UIView *)view {
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:view];
}

- (void)addConstraintsForFillView:(UIView *)view {
    [self addConstraintsOnAxis1:@"H:|-0-[V1]-0-|"
                          axis2:@"V:|-0-[V1]-0-|"
                        metrics:nil
                          views:@[view]];
}

- (void)addConstraintsOnAxis1:(NSString *)axis1
                        axis2:(NSString *)axis2
                      metrics:(NSArray *)metrics
                        views:(NSArray *)views {
    
    int i;
    
    i = 1;
    NSMutableDictionary *theMetrics = [NSMutableDictionary new];
    for (NSObject *o in metrics) {
        theMetrics[SWF(@"M%d", i++)] = o;
    }
    
    i = 1;
    NSMutableDictionary *theViews = [NSMutableDictionary new];
    for (NSObject *o in views) {
        theViews[SWF(@"V%d", i++)] = o;
    }
    
    if (axis1.length) {
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:axis1
                                                                     options:0
                                                                     metrics:theMetrics
                                                                       views:theViews]];
    }
    
    if (axis2.length) {
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:axis2
                                                                     options:0
                                                                     metrics:theMetrics
                                                                       views:theViews]];
    }
}

- (void)positionCenterHorizontal:(UIView*)view{
    NSLayoutConstraint *xCenterConstraint = [NSLayoutConstraint constraintWithItem:view
                                                                         attribute:NSLayoutAttributeCenterX
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self
                                                                         attribute:NSLayoutAttributeCenterX
                                                                        multiplier:1
                                                                          constant:0];
    
    [self addConstraint:xCenterConstraint];
}

- (void)positionCenterVertical:(UIView*)view{
    NSLayoutConstraint *yCenterConstraint = [NSLayoutConstraint constraintWithItem:view
                                                                         attribute:NSLayoutAttributeCenterY
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self
                                                                         attribute:NSLayoutAttributeCenterY
                                                                        multiplier:1
                                                                          constant:0];
    
    [self addConstraint:yCenterConstraint];
}

- (void)positionCenter:(UIView*)view{
    [self positionCenterHorizontal:view];
    [self positionCenterVertical:view];
}

-(void)alignView:(UIView*)viewToAlign withView:(UIView*)referenceView
{
    NSLayoutConstraint *leadingConstraint= [NSLayoutConstraint constraintWithItem:referenceView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:viewToAlign attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *trailingConstraint=[NSLayoutConstraint constraintWithItem:referenceView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:viewToAlign attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    NSLayoutConstraint *topConstraint=[NSLayoutConstraint constraintWithItem:referenceView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:viewToAlign attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *bottomConstraint=[NSLayoutConstraint constraintWithItem:referenceView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:viewToAlign attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    
    [self addConstraints:@[leadingConstraint,trailingConstraint,topConstraint,bottomConstraint]];
}


@end
