//
//  UIStoryboard+Additions.m
//  Template
//
//  Created by vineeth.thayyil on 09/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "UIStoryboard+Additions.h"

@implementation UIStoryboard (Additions)

+(UIStoryboard*) currentStoryBoard {
    
    return  [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

+(UIStoryboard*) settingsStoryBoard {
    
    return  [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
}

@end
