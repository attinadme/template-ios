//
//  UIView+UIAdditions.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 21/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIAdditions)

-(void) setCornerRadiusWithWidth:(float)value;

@end
