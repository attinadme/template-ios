//
//  UIColor+Additions.m
//  Template
//
//  Created by vineeth.thayyil on 07/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "UIColor+Additions.h"

@implementation UIColor (Additions)


+(UIColor*) homePageBackGroundColor {

    return [UIColor colorWithWhite:0.6 alpha:1.0f];
}


+(UIColor*) menuItemTextColor{
    //return [UIColor whiteColor];
  return [UIColor lightGrayColor];
}

+(UIColor*) menuBackgroundColor {
  return [UIColor colorFromHex:@"#d6d3d2"];// [UIColor colorWithRed:54.0f/255.0f green:54.0f/255.0f  blue:54.0f/255.0f  alpha:1.0f];
}
+(UIColor*) menuTableBackgroundColor {
    return [UIColor colorFromHex:@"#f2f2f2"];// [UIColor colorWithRed:54.0f/255.0f green:54.0f/255.0f  blue:54.0f/255.0f  alpha:1.0f];
}

+ (UIColor *) expandedMenuItemBackgroundColor {
    return [UIColor whiteColor];
}

+ (UIColor *) selectedColor
{
    return [UIColor lightGrayColor];
}


+ (UIColor *) menuItemSeperatorColor{
  return [UIColor grayColor];
  //return [UIColor whiteColor];

}

+ (UIColor *) signOutButtonBackgroundColour{
    return [UIColor colorWithRed:6.0f/255.0f  green:84.0f/255.0f blue:186.0f/255.0f alpha:1.0f]
;
    
}
+ (UIColor *) navigationBarColor {
    return [UIColor colorFromHex:@"#0064DC"];
    
}

+(UIColor *)colorFromHex:(NSString *)hexString {
    if(hexString){
        unsigned rgbValue = 0;
        NSScanner *scanner = [NSScanner scannerWithString:hexString];
        [scanner setScanLocation:1]; // bypass '#' character
        [scanner scanHexInt:&rgbValue];
        return [self colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
    }
    return [UIColor whiteColor];
}


@end
