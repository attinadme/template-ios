//
//  UIColor+Additions.h
//  Template
//
//  Created by vineeth.thayyil on 07/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Additions)

+ (UIColor *) homePageBackGroundColor;

+ (UIColor *) menuItemTextColor ;

+ (UIColor *) menuBackgroundColor ;

+ (UIColor*) menuTableBackgroundColor;

+ (UIColor *) expandedMenuItemBackgroundColor;

+ (UIColor *) selectedColor;

+ (UIColor *) menuItemSeperatorColor;

+ (UIColor *) signOutButtonBackgroundColour;

+ (UIColor *) navigationBarColor;

+ (UIColor *)colorFromHex:(NSString *)hexString;

@end
