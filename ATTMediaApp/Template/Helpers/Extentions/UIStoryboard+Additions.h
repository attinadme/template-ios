//
//  UIStoryboard+Additions.h
//  Template
//
//  Created by vineeth.thayyil on 09/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (Additions)

+(UIStoryboard*) currentStoryBoard;
+(UIStoryboard*) settingsStoryBoard;
@end
