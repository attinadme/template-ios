//
//  UIView+UIAdditions.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 21/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "UIView+UIAdditions.h"

@implementation UIView (UIAdditions)


-(void) setCornerRadiusWithWidth:(float)value  {
    [self.layer setCornerRadius:value];
    [self setClipsToBounds:YES];
}

@end
