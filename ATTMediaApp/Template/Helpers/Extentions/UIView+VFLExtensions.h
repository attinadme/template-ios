//
//  UIView+VFLExtensions.h


#import <UIKit/UIKit.h>
#define SWF(format, ...) [NSString stringWithFormat:format, ##__VA_ARGS__]
@interface UIView (VFLExtensions)

- (void)autoLayoutAddSubview:(UIView *)view;

- (void)addConstraintsForFillView:(UIView *)view;
- (void)addConstraintsOnAxis1:(NSString *)axis1
                        axis2:(NSString *)axis2
                      metrics:(NSArray *)metrics
                        views:(NSArray *)views;
- (void)positionCenterHorizontal:(UIView*)view;
- (void)positionCenterVertical:(UIView*)view;
- (void)positionCenter:(UIView*)view;
-(void)alignView:(UIView*)viewToAlign withView:(UIView*)referenceView;

@end
