//
//  Constants.m
//  Template
//
//  Created by vineeth.thayyil on 06/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//

#import "Constants.h"

@implementation Constants


 NSUInteger kSucessCode           = 200;
 NSUInteger kNotModifiedCode      = 304;

#pragma mark - Navigation -

NSString *kNavigationForcePushKey = @"needToforcePush"  ;
NSString *kNavigationTitle        = @"navigationTitle";

#pragma mark - Settings -

NSString *kUrlToLoadInWebView      = @"weblink";


@end
