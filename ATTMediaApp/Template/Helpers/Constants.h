//
//  Constants.h
//  Template
//
//  Created by vineeth.thayyil on 06/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Constants : NSObject

extern  NSUInteger kSucessCode           ;
extern  NSUInteger kNotModifiedCode      ;

#pragma mark - Navigation -

extern NSString * kNavigationForcePushKey  ;
extern NSString * kNavigationTitle;

#pragma mark - Settings -

extern  NSString *kUrlToLoadInWebView ;

@end
