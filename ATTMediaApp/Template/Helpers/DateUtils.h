//
//  DateUtils.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtils : NSObject

+ (NSDate*) fetchDateyyyyMMddTHHmmssZFrom:(NSString*)dateString;

+ (NSString*) fetchDateStringyyyyMMddTHHmmssZFrom:(NSDate*)date;

@end
