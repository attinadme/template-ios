//
//  Utils.m
//  Template
//
//  Created by vineeth.thayyil on 06/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (BOOL)isIpad
{
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
}

+(NSArray*) performCaseInSensitiveComparisonOn:(NSArray*)array
                                        forKey:(NSString*)key
                                     withValue:(NSString*)value {
    
    NSIndexSet *indexSet =  [array indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        
        NSString *actualValue = obj[key];
        
        if (actualValue && [[actualValue lowercaseString] isEqualToString:[value lowercaseString]]) {
            return YES;
        }
        
        return NO;
        
    }];
    
    NSArray *filtered = [array objectsAtIndexes:indexSet];
    return filtered;
}

@end
