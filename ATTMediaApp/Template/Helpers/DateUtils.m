//
//  DateUtils.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "DateUtils.h"

@implementation DateUtils

static NSString *const kSessionDateFormat           = @"yyyyMMdd'T'HH:mm:ssZ";


+ (NSDate*) fetchDateyyyyMMddTHHmmssZFrom:(NSString*)dateString {
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        [ dateFormatter setDateFormat: kSessionDateFormat ];
    });
    
    NSDate *date = [ dateFormatter dateFromString:dateString ];
    
    return date;
}


+ (NSString*) fetchDateStringyyyyMMddTHHmmssZFrom:(NSDate*)date {
    
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        [ dateFormatter setDateFormat: kSessionDateFormat ];
    });
    
    NSString *dateString = [ dateFormatter stringFromDate:date ];
    
    return dateString;
}




@end
