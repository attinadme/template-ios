//
//  AsynchronousBlockOperation.h
//
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^AsynOperationBlock)(void);

/**
 *  An asynchronous operation that will wait until itself being marked as finished upon completion of the block.
 */
@interface AsynchronousBlockOperation : NSOperation
@property (nonatomic, assign, readonly) BOOL isExecuting;
@property (nonatomic, assign, readonly) BOOL isFinished;

- (void) addOperationBlock: (AsynOperationBlock) block;
- (void) markAsFinished;

@end
