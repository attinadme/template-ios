//
//  AppDelegate.m
//  Template
//
//  Created by vineeth.thayyil on 07/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "AppDelegate.h"
#import "SplashViewController.h"
#import "MaintenanceViewController.h"
#import "AppLaunchManager.h"
#import "AppUiManager.h"
#import <ATTAnalytics/ATTAnalytics-Swift.h>

@interface AppDelegate ()

@property (nonatomic,strong) AppLaunchManager *launchManager;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
  
 
  
  
    CGRect screenFrame  = [ [ UIScreen mainScreen ] bounds ];
    self.window         = [ [ UIWindow alloc ] initWithFrame: screenFrame ];
    [self.window makeKeyAndVisible];
    self.window.backgroundColor = [UIColor blackColor];
   
    [self initializeAppLauncher];
    
    return YES;
}


-(void) initializeAppLauncher {
    
    self.window.rootViewController = [[ UIStoryboard currentStoryBoard ] instantiateViewControllerWithIdentifier:NSStringFromClass([SplashViewController class])];
    
    _launchManager = [ [ AppLaunchManager alloc ] init ];
    
    [self.launchManager startQueue:^{
        
        [self startApplication];
        
    } onFailure:^{
        self.window.rootViewController =  [[ UIStoryboard currentStoryBoard ] instantiateViewControllerWithIdentifier:NSStringFromClass([MaintenanceViewController class])];
    }];

  
  

}



-(void) startApplication {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.window.backgroundColor = [UIColor blackColor];
        AppUiManager *uiManager = [[AppUiManager alloc] init];
        self.window.rootViewController = [uiManager createUI];
      [[ATTAnalytics helper] beginTrackingWithPathForConfigFile:nil
                                              stateTrackingType:ATTAnalytics.TrackingTypeAuto
                                             actionTrackingType:ATTAnalytics.TrackingTypeAuto];

       
    });
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
