//
//  AppUiManager.m
//  Template
//
//  Created by vineeth.thayyil on 09/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "AppUiManager.h"
#import "PKRevealController.h"
#import "MenuViewController.h"
#import "HomeViewController.h"
#import "NavigationManager.h"

@implementation AppUiManager

- (UIViewController*) createUI {
        
    HomeViewController * mainVc = [[UIStoryboard currentStoryBoard] instantiateViewControllerWithIdentifier:NSStringFromClass([HomeViewController class])];
    
    UINavigationController *navigationVc = [[UINavigationController alloc] initWithRootViewController:mainVc];

    navigationVc.navigationItem.hidesBackButton = YES;
    
    MenuViewController *menuVC = [[MenuViewController alloc] init];
    PKRevealController *rootVc = [PKRevealController revealControllerWithFrontViewController:navigationVc leftViewController:menuVC];
    
    float width = [Utils isIpad] ? 300 : [UIScreen mainScreen].bounds.size.width - 70;
    
    [rootVc setMinimumWidth:width maximumWidth:[UIScreen mainScreen].bounds.size.width   forViewController:menuVC ];
    rootVc.recognizesPanningOnFrontView = YES;
  
    
    [NavigationManager sharedInstance].navigationVc = navigationVc;
    [NavigationManager sharedInstance].menuVC       = menuVC;
    [NavigationManager sharedInstance].delegate     = mainVc;
    [NavigationManager sharedInstance].revealController  = rootVc;

    
    return rootVc;
}

@end
