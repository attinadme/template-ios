//
//  AppDelegate.h
//  Template
//
//  Created by vineeth.thayyil on 07/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

-(void) initializeAppLauncher;

@end

