//
//  AppLaunchManager.m
//  Template
//
//  Created by vineeth.thayyil on 09/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "AppLaunchManager.h"
#import "AsynchronousBlockOperation.h"

@interface AppLaunchManager ( )

@property (nonatomic, copy) LaunchManagerBlock completionBlock;
@property (nonatomic, copy) LaunchManagerBlock failureBlock;
@property (nonatomic, assign) BOOL errorTriggered;
@property (nonatomic, strong) NSOperationQueue *queue;

@end

@implementation AppLaunchManager

- (instancetype) init
{
    self = [ super init ];
    
    if( self )
    {
        _queue = [ [ NSOperationQueue alloc ] init ];
        _queue.maxConcurrentOperationCount = 5;
        _errorTriggered = NO;
    }
    
    return self;
}


- (void) startQueue:(LaunchManagerBlock) completionBlock
          onFailure:(LaunchManagerBlock) failureBlock {
    
    self.completionBlock = completionBlock;
    self.failureBlock    = failureBlock;
    [self performSelector:@selector(notifySuccess) withObject:nil afterDelay:5.0f];
    
}


- (void) notifySuccess
{
    if( self.completionBlock )
    {
        self.completionBlock( );
    }
}


- (void) notifyError
{
    self.errorTriggered = YES;

    if( self.failureBlock )
    {
        self.failureBlock( );
    }
}



@end
