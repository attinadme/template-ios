//
//  AppLaunchManager.h
//  Template
//
//  Created by vineeth.thayyil on 09/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^LaunchManagerBlock)(void);

@interface AppLaunchManager : NSObject

- (void) startQueue:(LaunchManagerBlock) completionBlock
          onFailure:(LaunchManagerBlock) failureBlock;


@end
