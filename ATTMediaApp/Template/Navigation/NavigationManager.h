//
//  NavigationManger.h
//  Template
//
//  Created by vineeth.thayyil on 09/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NavigationDestination.h"

typedef NS_ENUM(NSUInteger, NavigationHeaderStyle) {
    NavigationHeaderStyleBack,
    NavigationHeaderStyleMenu,
    NavigationHeaderStyleSearch,
    NavigationHeaderStyleNone
    
};





@interface NavigationManager : UIViewController

@property (nonatomic,strong) UINavigationController *navigationVc;
@property (nonatomic,strong) UIViewController *menuVC;
@property (nonatomic,weak) id<NavigationManagerDelegate> delegate;
@property (nonatomic,strong) PKRevealController *revealController;

+(instancetype) sharedInstance ;


-(void) navigateToView:(NavigationDestinationType)destination
              withData:(NSDictionary*)data
           isforcePush:(BOOL)isforcePush;

-(void) navigateToDestination:(NavigationDestination*)destination;

- (void) configureNavigationBarForCurrentController;

-(void)hideMenu;

@end
