//
//  NavigationManger.m
//  Template
//
//  Created by vineeth.thayyil on 09/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "NavigationManager.h"
#import "PKRevealController.h"
#import "BaseViewController.h"
#import "HomeViewController.h"
#import "SettingsViewController.h"
#import "WebViewController.h"

@interface NavigationManager ()


@end

@implementation NavigationManager

+(instancetype) sharedInstance
{
    static NavigationManager *_sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [ [ self alloc ] init ];
    });
    
    return _sharedManager;
}


- (void) setRevealController:(PKRevealController *)revealController
{
    _revealController = revealController;
    [ self configureNavigationBarForCurrentController ];
}

-(void) navigateToDestination:(NavigationDestination*)destination {
    
    BOOL forcePush = [destination.metadata [kNavigationForcePushKey] boolValue];
    [self navigateToView:destination.type
                withData:destination.metadata
             isforcePush:forcePush];
    
}


-(void) navigateToView:(NavigationDestinationType)destination
              withData:(NSDictionary*)data
           isforcePush:(BOOL)isforcePush {
    
    BaseViewController *vc;
    
    switch (destination) {
            
        case NavigationDestinationTypeHome:
            vc = [[UIStoryboard currentStoryBoard] instantiateViewControllerWithIdentifier:NSStringFromClass([HomeViewController class])];;
            break;
        case NavigationDestinationTypeSettings:
            vc = [[UIStoryboard settingsStoryBoard] instantiateViewControllerWithIdentifier:NSStringFromClass([SettingsViewController class])];
            break;
            
        case  NavigationDestinationTypeWebView:
            vc = [[WebViewController alloc] init];
            break;
        default:
            break;
    }
    
    vc.data = data;
    
    if (isforcePush) {
        [self.navigationVc setViewControllers:@[vc] animated:YES];
    }else{
        [self.navigationVc pushViewController:vc animated:YES];
    }
    
    self.delegate = vc;
    [ self configureNavigationBarForCurrentController ];
    
    [ self.revealController showViewController:self.navigationController ];


}



- (void) configureNavigationBarForCurrentController
{
    
    if (![self.delegate respondsToSelector:@selector(headerStyle)]) {
        return;
    }
    
    HeaderStyle desiredHeaderStyle = [ self.delegate headerStyle ];

    if( desiredHeaderStyle & HeaderStyleRevealButton )
    {
        [ self addRevealButton ];
    }

}


- (void) addRevealButton
{
    UIBarButtonItem *menuItem = [[ UIBarButtonItem alloc ] initWithImage: nil style: UIBarButtonItemStylePlain target: self action: @selector( revealMenu:) ];
   UIImage *image = [[UIImage imageNamed:@"Menu_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
   [menuItem setImage:image];
    self.navigationVc.visibleViewController.navigationItem.leftBarButtonItem = menuItem;
}



-(void)showRevealMenu
{
    [self revealMenu:nil];
}

- (IBAction) revealMenu:(id)sender
{
    [ self.revealController showViewController:self.menuVC ];
}

-(void)hideMenu
{
    [ self.revealController showViewController:self.navigationVc];
}



@end
