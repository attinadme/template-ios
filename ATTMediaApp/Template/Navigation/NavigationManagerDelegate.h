//
//  NavigationManagerDelegate.h
//  Wall
//
//  Created by vineeth.thayyil on 10/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_OPTIONS(NSInteger, HeaderStyle) {
    HeaderStyleLogo = 1 << 0,
    HeaderStyleTitle = 1 << 1,
    HeaderStyleGoingBack = 1 << 2,
    HeaderStyleRevealButton = 1 << 3,
    HeaderStyleSecondScreen = 1 << 4,
    HeaderStyleNone = 1 << 5,
    HeaderStyleSearchButton = 1 << 6,
    HeaderStyleNoRightBarButton = 1 << 7
};


@protocol NavigationManagerDelegate <NSObject>

- (HeaderStyle) headerStyle;

@optional

- (NSString*) title;

@end
