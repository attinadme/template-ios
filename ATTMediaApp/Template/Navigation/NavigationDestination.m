//
//  NavigationDestination.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 21/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "NavigationDestination.h"

@interface NavigationDestination()

@property (nonatomic, assign, readwrite) NavigationDestinationType type;
@property (nonatomic, strong, readwrite) NSDictionary *metadata;

@end

@implementation NavigationDestination

- (instancetype)initWithType:(NavigationDestinationType)type metadata:(NSDictionary *)metadata
{
    self = [super init];
    if (self)
    {
        _type = type;
        _metadata = metadata;
    }
    return self;
}

- (void)addToMetadata:(NSDictionary *)keyValuePair
{
    NSMutableDictionary *newDictionary = self.metadata ? self.metadata.mutableCopy : [[NSMutableDictionary alloc] init];
    [newDictionary addEntriesFromDictionary: keyValuePair];
    _metadata = newDictionary;
}

@end
