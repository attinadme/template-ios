//
//  NavigationDestination.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 21/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>



typedef NS_ENUM(NSUInteger, NavigationDestinationType) {
    NavigationDestinationTypeHome,
    NavigationDestinationTypeSettings,
    NavigationDestinationTypeWebView
};



@interface NavigationDestination : NSObject


@property (nonatomic, assign, readonly) NavigationDestinationType type;
@property (nonatomic, strong, readonly) NSDictionary *metadata;


/**
 *  Designated Initializer.
 */
- (instancetype)initWithType: (NavigationDestinationType)type metadata: (NSDictionary *)metadata;

/**
 *  Addes extra metadata into the exisitng metadata.
 */
- (void)addToMetadata: (NSDictionary *) newMetadata;

@end
