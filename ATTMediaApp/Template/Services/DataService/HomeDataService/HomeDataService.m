//
//  HomeDataService.m
//  AttinadTemplate
//
//  Created by vineeth.thayyil on 07/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//

#import "HomeDataService.h"
#import "HomeApiServices.h"
#import "HomeApiService.h"

@interface HomeDataService ()

@property (nonatomic,strong) id<HomeApiServices> apiService;

@end

@implementation HomeDataService


-(instancetype) init {
    
    self = [ super init ];
    if( self )
    {
        _apiService = [[HomeApiService alloc] initWithEndPoint:kMiddlewareUrl];
    }
    
    return self;
}



-(void) fetchDetailsOnCompletion:(void (^)(id response, BOOL status, NSError *error))completionBlock {
    
    [self.apiService fetchDetailsOnCompletion:^(id response, BOOL status, NSError *error) {
        
    }];
}

@end
