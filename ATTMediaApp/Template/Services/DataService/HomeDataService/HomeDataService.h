//
//  HomeDataService.h
//  Template
//
//  Created by vineeth.thayyil on 07/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//

#import "BaseService.h"

@interface HomeDataService : BaseService

-(void) fetchDetailsOnCompletion:(void (^)(id response, BOOL status, NSError *error))completionBlock;

@end
