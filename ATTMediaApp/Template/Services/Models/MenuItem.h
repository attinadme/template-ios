//
//  MenuItem.h
//  Template
//
//  Created by vineeth.thayyil on 14/09/16.
//  Copyright © 2016 vineeth.thayyil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger, MenuSelectionType) {
    MenuSelectionTypeHome
};

@interface MenuItem : NSObject

@property (nonatomic,strong) UIImage *image;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,assign) MenuSelectionType menuSelectionType;
@property (nonatomic, strong) NSArray *subMenuItems;
@property (nonatomic, assign) BOOL isOpened;
@property (nonatomic, assign) BOOL hasChildren;

@end
