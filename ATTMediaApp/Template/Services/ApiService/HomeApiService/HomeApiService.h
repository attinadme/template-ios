//
//  HomeApiService.h
//  Template
//
//  Created by vineeth.thayyil on 06/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//

#import "BaseService.h"
#import "HomeApiServices.h"


static NSString *const kMiddlewareUrl = @"";


@interface HomeApiService : BaseService <HomeApiServices>


@end
