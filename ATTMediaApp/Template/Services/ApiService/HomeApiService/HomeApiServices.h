//
//  HomeApiServices.h
//  Template
//
//  Created by vineeth.thayyil on 06/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HomeApiServices <NSObject>

-(void) fetchDetailsOnCompletion:(void (^)(id response, BOOL status, NSError *error))completionBlock;

@end
