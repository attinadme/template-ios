//
//  HomeApiService.m
//  Template
//
//  Created by vineeth.thayyil on 06/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//

#import "HomeApiService.h"

static NSString *const kGetHomePageDetails = @"";


@implementation HomeApiService

-(void) fetchDetailsOnCompletion:(void (^)(id response, BOOL status, NSError *error))completionBlock  {
    
 
    [self sendGETRequest:kGetHomePageDetails
         completionBlock:^(NSDictionary *response) {
             
             completionBlock(response,YES,nil);
        
    } onFailure:^(NSDictionary *response, NSError *error) {
        completionBlock(response,NO,error);

    }];
}

@end
