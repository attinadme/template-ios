//
//  ConfigurationApiService.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "ConfigurationApiService.h"

static NSString *const kFetchSession  = @"";
static NSString *const kFetchMetadata = @"";

@implementation ConfigurationApiService

-(void) fetchSessionOnCompletion:(void (^)(id response, BOOL status, NSError *error))completionBlock  {
    
    
    [self sendGETRequest:kFetchSession
         completionBlock:^(NSDictionary *response) {
             
             completionBlock(response,YES,nil);
             
         } onFailure:^(NSDictionary *response, NSError *error) {
             completionBlock(response,NO,error);
             
         }];
}


static NSString *const kXSessionHeader          = @"X-Session";
static NSString *const kIfModifiedSinceHeader   = @"If-Modified-Since";


-(void) fetchMetaDataForSession:(NSString*)session
              WithLastSavedTime:(NSString*)lastSavedTime
                   OnCompletion:(void (^)(id response, NSUInteger statusCode, NSError *error))completionBlock  {
    
    
    
    NSString *postUrlString            = @"";
    NSMutableURLRequest *request    = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:postUrlString]];
    
    [request setValue:session forHTTPHeaderField:kXSessionHeader];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    if (lastSavedTime) {
        [request setValue:lastSavedTime forHTTPHeaderField:kIfModifiedSinceHeader];
    }
    
    NSURLSessionDataTask *task =   [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable connectionError) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        id responseObject = nil;
        
        if (data) {
            responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock (responseObject,[httpResponse statusCode],connectionError);
            
        });

    }];
    
    [task resume];
    
}



-(void) downloadLanguageFileWithUrl:(NSString*)urlString
                       onCompletion:(void (^)(id response, BOOL status, NSError *error))completionBlock {
    
    NSMutableURLRequest *request    = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    
    
    NSURLSessionDataTask *task =   [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable connectionError) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        id responseObject = nil;
        
        if (data) {
            responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if ([httpResponse statusCode] == kSucessCode ) {
                completionBlock (responseObject,YES,connectionError);

            }else{
                completionBlock (responseObject,NO,connectionError);

            }
            
            
        });
        
    }];
    
    [task resume];
    

}


@end
