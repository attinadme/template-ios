//
//  ConfigurationApiService.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "BaseService.h"
#import "ConfigurationApiServices.h"

@interface ConfigurationApiService : BaseService <ConfigurationApiServices>

@end
