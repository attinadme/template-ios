//
//  ConfigurationApiServices.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ConfigurationApiServices <NSObject>

-(void) fetchSessionOnCompletion:(void (^)(id response, BOOL status, NSError *error))completionBlock;


-(void) fetchMetaDataForSession:(NSString*)session
              WithLastSavedTime:(NSString*)lastSavedTime
                   OnCompletion:(void (^)(id response, NSUInteger statusCode, NSError *error))completionBlock;


-(void) downloadLanguageFileWithUrl:(NSString*)urlString
                       onCompletion:(void (^)(id response, BOOL status, NSError *error))completionBlock;

@end
