//
//  BaseService.h
//  Template
//
//  Created by vineeth.thayyil on 06/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^SuccessBlock)(NSDictionary* response);
typedef void (^ErrorBlock)(NSDictionary* response,NSError* error);



@interface BaseService : NSObject


- (instancetype) initWithEndPoint: (NSString *) endPoint;


- (void) sendGETRequest: (NSString *) requestSuffix
        completionBlock: (SuccessBlock) completionBlock
              onFailure: (ErrorBlock) failureBlock;

- (void) sendDELETERequest: (NSString *)requestSuffix
                parameters:(id)parameters
           completionBlock: (SuccessBlock)completionBlock
                 onFailure: (ErrorBlock) failureBlock;

- (void) sendPostRequest: (NSString *)requestSuffix
              parameters:(id)parameters
         completionBlock: (SuccessBlock)completionBlock
               onFailure: (ErrorBlock) failureBlock;

@end
