//
//  BaseService.m
//  Template
//
//  Created by vineeth.thayyil on 06/02/17.
//  Copyright © 2017 vineeth.thayyil. All rights reserved.
//

#import "BaseService.h"

static NSString *const kGetMethod       =   @"GET";
static NSString *const kPostMethod      =   @"POST";
static NSString *const kPutMethod       =   @"PUT";
static NSString *const kDeleteMethod    =   @"DELETE";

static NSString *const kEndPointAssertionMessage= @"The endPoint you are trying to set is nil";
static NSString *const kRequestHeaderAccept     = @"Accept";
static NSString *const kApplicationJsonValue    = @"application/json";
static NSString *const kContentType             = @"Content-Type";



static float kTimeOutInterval   = 60.0f;


@interface BaseService()

@property (nonatomic, strong) NSString *endPoint;

@end


@implementation BaseService


- (instancetype) initWithEndPoint: (NSString *) endPoint
{
    NSAssert( endPoint != nil, kEndPointAssertionMessage );
    
    self = [ super init ];
    if( self )
    {
        _endPoint = endPoint;
        
    }
    return self;
    
}



#pragma mark - HTTP request

/**
 *  Send a GET request .
 *  Cache policy set to default.
 *
 *  @param requestSuffix    a NSString representing the request suffix
 *  @param completionBlock  block to be executed when the request is completed
 *  @param failureBlock     block to be executed on error
 */

- (void) sendGETRequest: (NSString *) requestSuffix
        completionBlock: (SuccessBlock) completionBlock
              onFailure: (ErrorBlock) failureBlock
{
    
    NSString *urlString             = [_endPoint stringByAppendingString:[NSString stringWithFormat:@"/%@",requestSuffix]];
    
    NSURL *url                      = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request    = [NSMutableURLRequest requestWithURL:url
                                                              cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                          timeoutInterval:kTimeOutInterval];
    [request setHTTPMethod:kGetMethod];
    [request setValue:kApplicationJsonValue forHTTPHeaderField:kContentType];


    NSURLSessionDataTask *task =   [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable connectionError) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        id responseObject = nil;
        
        if (data) {
            responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
        
        if ([httpResponse statusCode] == kSucessCode ) {
            
            if( completionBlock ) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock( responseObject );

                });
            }
        }else{
            
            if(failureBlock){
                dispatch_async(dispatch_get_main_queue(), ^{
                    failureBlock(responseObject,connectionError);
                });
            }
        }
        
    }];
    
    [task resume];
    
    
}

/**
 *  Send a DELETE request.
 *
 *  @param requestSuffix    a NSString representing the request suffix
 *  @param completionBlock  block to be executed when the request is completed
 *  @param failureBlock     block to be executed on error
 */

- (void) sendDELETERequest: (NSString *)requestSuffix
                parameters:(id)parameters
           completionBlock: (SuccessBlock)completionBlock
                 onFailure: (ErrorBlock) failureBlock
{
    
    
    NSString *urlString             = [_endPoint stringByAppendingString:[NSString stringWithFormat:@"/%@",requestSuffix]];
    
    NSURL *url                      = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request    = [NSMutableURLRequest requestWithURL:url
                                                              cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                          timeoutInterval:kTimeOutInterval];
    
    [request setHTTPMethod:kDeleteMethod];
    [request setValue:kApplicationJsonValue forHTTPHeaderField:kContentType];

    
    [[NSURLSession sharedSession] dataTaskWithRequest:request
                                    completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable connectionError) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        
        id responseObject = nil;
        
        if (data) {
            responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
        if ([httpResponse statusCode] == kSucessCode ) {
            
            if( completionBlock ) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock( responseObject );
                    
                });
                
            }
        }else{
            
            if(failureBlock){
                dispatch_async(dispatch_get_main_queue(), ^{
                    failureBlock(responseObject,connectionError);
                });
            }
        }
        
    }];
    
    
    
}

/**
 *  Send a POST request
 *  @param parameters       Parameters to post
 *  @param requestSuffix    a NSString representing the request suffix
 *  @param completionBlock  block to be executed when the request is completed
 *  @param failureBlock     block to be executed on error
 */

- (void) sendPostRequest: (NSString *)requestSuffix
              parameters: (id)parameters
         completionBlock: (SuccessBlock)completionBlock
               onFailure: (ErrorBlock) failureBlock {
    
    
    NSString *urlString             = [_endPoint stringByAppendingString:[NSString stringWithFormat:@"/%@",requestSuffix]];
    
    NSURL *url                      = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request    = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:kTimeOutInterval];
    
    [request setHTTPMethod:kPostMethod];
    [request setValue:kApplicationJsonValue forHTTPHeaderField:kContentType];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters
                                                       options:0
                                                         error:nil];
    
    
    [request setHTTPBody:jsonData];

    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable connectionError) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        
        id responseObject = nil;
        
        if (data) {
            responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
        if ([httpResponse statusCode] == kSucessCode ) {
            
            if( completionBlock ) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock( responseObject );
                    
                });
                
            }
        }else{
            
            if(failureBlock){
                dispatch_async(dispatch_get_main_queue(), ^{
                    failureBlock(responseObject,connectionError);
                });
            }
        }
        
    }];
    
    [task resume];
    
}



@end
