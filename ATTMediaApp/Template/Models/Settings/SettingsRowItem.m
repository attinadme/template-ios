//
//  SettingsRowItem.m
//  SonyLiv
//
//  Created by Vineeth S on 9/30/15.
//  Copyright (c) 2015 Accedo. All rights reserved.
//

#import "SettingsRowItem.h"

@implementation SettingsRowItem


-(instancetype) initWithTitle:(NSString*)title
                     subTitle:(NSString*)subTitle
                    urlString:(NSString*)urlString
                     itemtype:(SettingsItemtype)itemType {
    
    if (self = [super init]) {
        
        _mainTitle = title;
        _subTitle  = subTitle;
        _itemType  = itemType;
        _urlString = urlString;
    }
    return self;
}

@end
