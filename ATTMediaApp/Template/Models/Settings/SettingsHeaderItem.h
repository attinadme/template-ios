//
//  SettingsHeaderItem.h
//  SonyLiv
//
//  Created by Vineeth S on 9/30/15.
//  Copyright (c) 2015 Accedo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingsHeaderItem : NSObject

@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSMutableArray *subItems;

@end
