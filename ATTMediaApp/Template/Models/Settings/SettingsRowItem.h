//
//  SettingsRowItem.h
//  SonyLiv
//
//  Created by Vineeth S on 9/30/15.
//  Copyright (c) 2015 Accedo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SettingsItemtype) {
    SettingsItemtypeDefault,
    SettingsItemtypeWebview
};

@interface SettingsRowItem : NSObject

@property (nonatomic,strong) NSString *mainTitle;
@property (nonatomic,strong) NSString *subTitle;
@property (nonatomic,strong) NSString *urlString;

@property (nonatomic,assign) SettingsItemtype itemType;


-(instancetype) initWithTitle:(NSString*)title
                     subTitle:(NSString*)subTitle
                    urlString:(NSString*)urlString
                     itemtype:(SettingsItemtype)itemType;

@end
