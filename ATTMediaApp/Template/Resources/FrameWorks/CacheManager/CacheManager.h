//
//  CacheManager.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheManager : NSObject


/**
 *  For caching an item
 *
 *  @param object       The object to cache
 *  @param key          The uniue key
 *  @param timeInterval Cache valid time in seconds. Specify 0 if it doesn't expires
 */

-(void) cacheObject:(id) object forKey:(NSString*) key expirationTime:(NSTimeInterval)timeInterval ;

/**
 *  For fetching the cached deatils. If cached details is presend and is valid will return it. If its invalid will clear the cache.
 *
 *  @param key unique Key
 *
 *  @return Cached details
 */

-(id) fetchCachedDataForKey:(NSString*) key ;

/**
 *  Clear the cache saved for a particular key
 *
 *  @param key unique key
 */

-(void) clearCacheForKey:(NSString*) key ;




@end
