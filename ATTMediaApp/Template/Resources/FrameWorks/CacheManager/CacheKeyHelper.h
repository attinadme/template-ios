//
//  CacheKeyHelper.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheKeyHelper : NSObject


+ (NSString*) cacheKeyForMethod: (NSString *) methodName
                     identifier: (NSString *) identifier;

+ (NSString *) cacheKeyForRequestUrl: (NSString *)requestString;

+ (NSString *) cacheKeyForMethod: (NSString *) methodName
                      parameters: (NSDictionary*) parameters;

@end
