//
//  PersistantCacheManager.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 *  For persistant storege of details inisde the application.Data stored using this calls will get saved in a file. So that it won't get cleared if the app is killed
 */


@interface PersistantCacheManager : NSObject

/**
 *  For storing the details into a file
 *
 *  @param object the dictionary object to cache
 *  @param key    unique key
 */

+(void) cacheObject:(NSDictionary*) object forKey:(NSString*) key;

/**
 *  For fetching the details saved earlier
 *
 *  @param key unique key
 *
 *  @return the cached data
 */
+(NSDictionary*) fetchCachedDetailsForKey:(NSString*)key;


/**
 *  For clearing the cache
 *
 *  @param key unique key
 */

+(void) clearCacheForKey:(NSString*) key;

@end
