//
//  CacheKeyHelper.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "CacheKeyHelper.h"

static NSString *const kDash                   = @"-";
static NSString *const kPageCacheKeyStepFormat = @"%@:%@";

@implementation CacheKeyHelper

+ (NSString*) cacheKeyForMethod: (NSString *) methodName
                     identifier: (NSString *) identifier
{
    if( !identifier )
    {
        return nil;
    }
    
    NSString *returnValue = [ [ [ methodName stringByAppendingString: kDash ]  stringByAppendingString: kDash ] stringByAppendingString: identifier ];
    return [ [ returnValue dataUsingEncoding: NSUTF8StringEncoding ] base64EncodedStringWithOptions: 0 ];
}

+ (NSString *) cacheKeyForMethod: (NSString *) methodName
                      parameters: (NSDictionary*) parameters
{
    NSArray *allKeys = [ [ parameters allKeys ] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:) ];
    
    NSString *returnValue = methodName;
    
    for( NSString *key in allKeys )
    {
        NSString *value = parameters[ key ];
        
        returnValue = [ [ returnValue stringByAppendingString: kDash ] stringByAppendingFormat: kPageCacheKeyStepFormat, key, value ];
    }
    
    return [ [ returnValue dataUsingEncoding: NSUTF8StringEncoding ] base64EncodedStringWithOptions: 0 ];
}

+ (NSString *) cacheKeyForRequestUrl: (NSString *)requestString
{
    return [[requestString dataUsingEncoding: NSUTF8StringEncoding] base64EncodedStringWithOptions: kNilOptions];
}

@end
