//
//  PersistantCacheManager.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "PersistantCacheManager.h"

static NSString *kLibraryFolderName     = @"Library";
static NSString *kCacheFolderName       = @"CacheFolder";

@implementation PersistantCacheManager

+(BOOL) checkFileExistsAtPath:(NSString*)path {
    return  [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:nil];
}

+(void) createDirectryAtPath:(NSString*)path {
    
    NSError *error = nil;
    
    if ([[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error]) {
        
    } else {
        NSLog(@"Folder creation failed %@",error.description);
    }
}

+(NSString *) fetchLibraryFolderPath {
    NSString *libraryPath = [NSHomeDirectory() stringByAppendingPathComponent:kLibraryFolderName];
    return libraryPath;
}


+(NSString*) fetchCacheFolderPath {
    NSString *languageFolder = [[self fetchLibraryFolderPath] stringByAppendingPathComponent:kCacheFolderName];
    return languageFolder;
}

+(NSString*) fetchCacheFilePathForKey:(NSString*) key {
    
    NSString *cacheFolderPath = [self fetchCacheFolderPath];
    
    if (![self checkFileExistsAtPath:cacheFolderPath]) {
        [self createDirectryAtPath:cacheFolderPath];
    }
    key = [key stringByReplacingOccurrencesOfString:@"/" withString:@""];
    key = [key stringByReplacingOccurrencesOfString:@":" withString:@""];
    return [cacheFolderPath stringByAppendingPathComponent:key];
}

+(BOOL) checkCacheExistsForKey:(NSString*)key {
    NSString *fileCachePath = [self fetchCacheFilePathForKey:key];
    return [self checkFileExistsAtPath:fileCachePath];
}

#pragma mark - Public Functions - 

+(void) cacheObject:(NSDictionary*) object forKey:(NSString*) key {
    
    if (!object) {
        return;
    }
    
    NSString *fileCachePath = [self fetchCacheFilePathForKey:key];
    [object writeToFile:fileCachePath atomically:NO];
}

+(NSDictionary*) fetchCachedDetailsForKey:(NSString*)key {
    
    if ([self checkCacheExistsForKey:key]) {
        
        NSString *cachedFilePath = [self fetchCacheFilePathForKey:key];
        return [NSDictionary dictionaryWithContentsOfFile:cachedFilePath];
    }
    
    return nil;
}

+(void) clearCacheForKey:(NSString*) key {
    
    NSString *fileCachePath = [self fetchCacheFilePathForKey:key];
    [[NSFileManager defaultManager] removeItemAtPath:fileCachePath error:nil];

}

@end
