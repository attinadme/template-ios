//
//  CacheModel.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "CacheModel.h"

@implementation CacheModel


-(instancetype) initWithValue:(id) value expirationInterval:(NSTimeInterval)interval {
    
    
    if (self = [super init]) {
        
        self.cacheValue = value;
        
        if (interval > 0) {
            self.expirationDate = [[NSDate date] dateByAddingTimeInterval:interval];

        }
        
    }
    
    return self;
}

-(BOOL) isValid {
    
    if (self.expirationDate ) {
        
        if ([self.expirationDate compare:[NSDate date]] == NSOrderedAscending) {
            return NO;
        }else{
            return YES;
        }
        

    }
    
    return YES;
}


@end
