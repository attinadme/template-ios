//
//  CacheModel.h
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheModel : NSObject


@property (nonatomic,strong) NSDate *expirationDate ;
@property (nonatomic,strong) id cacheValue;


-(instancetype) initWithValue:(id) value expirationInterval:(NSTimeInterval)interval;


-(BOOL) isValid;


@end
