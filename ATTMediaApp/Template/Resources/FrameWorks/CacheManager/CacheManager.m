//
//  CacheManager.m
//  ATTMediaApp
//
//  Created by vineeth.thayyil on 17/02/17.
//  Copyright © 2017 Attinad. All rights reserved.
//

#import "CacheManager.h"
#import "CacheModel.h"

@interface CacheManager ()

@property (nonatomic,strong) NSCache *cache;

@end


@implementation CacheManager


-(instancetype) init {
    
    if (self = [super init]) {
        _cache = [[NSCache alloc] init];
    }
    
    return self;
}



-(void) cacheObject:(id) object forKey:(NSString*) key expirationTime:(NSTimeInterval)timeInterval {
    
    if (key && object) {
        CacheModel *cacheModel = [[CacheModel alloc] initWithValue:object expirationInterval:timeInterval];
        [self.cache setObject:cacheModel forKey:key];
    }
}



-(id) fetchCachedDataForKey:(NSString*) key {
    
    CacheModel *model = [self.cache objectForKey:key];
    
    if (model) {
        
        if ([model isValid]) {
            return model.cacheValue;
        }else{
            [self clearCacheForKey:key];
        }
        
    }
    
    return nil;
}


-(void) clearCacheForKey:(NSString*) key {
    
    CacheModel *model = [self.cache objectForKey:key];
    if (model) {
        [self.cache removeObjectForKey:key];
    }

}







@end
