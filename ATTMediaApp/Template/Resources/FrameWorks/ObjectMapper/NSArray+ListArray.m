//
//  NSDictionary+Additions.m
//
//
//  Created by vineeth.thayyil on 10/03/16.
//
//
#import "NSArray+ListArray.h"
#import "NSObject+KeyList.h"
#import "NSDictionary+ObjectRepresentation.h"

@implementation NSArray (ListArray)


-(NSMutableArray*)arrayList {
    
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    
    for (id obj in self) {
        
        if ([obj  isKindOfClass:[NSNumber class]]) {
            [dataArray addObject:obj];
        }else if ([obj  isKindOfClass:[NSString class]]) {
            [dataArray addObject:obj];
        }else if ([obj  isKindOfClass:[NSArray class]]) {
            [dataArray addObject:[obj arrayList]];
        }else {
            [dataArray addObject:[obj dictionary]];
        }

    }
    return dataArray;
}




-(id)objectRepresentationForClass:(NSString*)className customKeys:(NSDictionary*)keyDetails{
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    
    for (id obj in self) {
        
        if ([obj  isKindOfClass:[NSNumber class]]) {
            [dataArray addObject:obj];
        }else if ([obj  isKindOfClass:[NSString class]]) {
            [dataArray addObject:obj];
        }else if ([obj  isKindOfClass:[NSArray class]]) {
            [dataArray addObject:obj];
        }else if ([obj  isKindOfClass:[NSDictionary class]]) {
            [dataArray addObject:[obj objectRepresentationForClass:className customKeys:keyDetails]];
        }
        
    }
    return dataArray;
}




@end
