//
//  NSDictionary+Additions.m
//
//
//  Created by vineeth.thayyil on 10/03/16.
//
//

#import <Foundation/Foundation.h>


static NSString *const kResponseKey = @"responseKey";
static NSString *const kModelClassName = @"modelClassName";

@interface NSDictionary (ObjectRepresentation)

/**
 *  For converting a dictionary to corresponding dictionary model object class
 */

-(id)objectRepresentationForClass:(NSString*)className customKeys:(NSDictionary*)keyDetails;

@end
