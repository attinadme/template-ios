//
//  NSDictionary+Additions.m
//
//
//  Created by vineeth.thayyil on 10/03/16.
//
//

#import <Foundation/Foundation.h>
/**
 *  For converting array of dictionaries to array of corresponding classes and vice versa
 */
@interface NSArray (ListArray)


-(id)objectRepresentationForClass:(NSString*)className customKeys:(NSDictionary*)keyDetails ;


-(NSMutableArray*)arrayList;

@end
