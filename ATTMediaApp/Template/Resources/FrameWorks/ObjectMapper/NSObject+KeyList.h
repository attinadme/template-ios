//
//  NSDictionary+Additions.m
//
//
//  Created by vineeth.thayyil on 10/03/16.
//
//

#import <Foundation/Foundation.h>

@interface NSObject (KeyList)


/**
 *  For converting a model object class to corresponding dictionary
 */

-(NSMutableDictionary*)dictionary ;


-(NSMutableArray*)getAllKeyNames;

@end
